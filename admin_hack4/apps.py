from django.apps import AppConfig


class AdminHack4Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admin_hack4'
