from django.contrib import admin

# Register your models here.
from admin_hack4.models import Medical, Specialty, Schedule, MedicalSpeciality, Appointment, Patient


class MedicalAdmin(admin.ModelAdmin):
    list_display = ('name', 'last_name', 'dni')


class SpecialtyAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


class MedicalSpecialityAdmin(admin.ModelAdmin):
    list_display = ('speciality_id', 'medical_id')


class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('medical_id', 'patient_id', 'date_of_attention')


class PatientAdmin(admin.ModelAdmin):
    list_display = ('name', 'last_name', 'dni', 'sex')


class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('medical_id', 'date_of_attention')


admin.site.register(Medical, MedicalAdmin)
admin.site.register(Specialty)
admin.site.register(Schedule)
admin.site.register(MedicalSpeciality)
admin.site.register(Appointment)
admin.site.register(Patient)

