from rest_framework import serializers
from .models import Medical, Specialty, Schedule, MedicalSpeciality, Appointment, Patient


class MedicalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medical
        fields = ['id_medical', 'name', 'last_name', 'dni', 'address', 'email', 'phone', 'sex', 'tuition_number',
                  'date_of_birth', 'registration_date', 'modification_date', 'registration_user', 'modification_user',
                  'active']


class SpecialtySerializer(serializers.ModelSerializer):
    class Meta:
        model = Specialty
        fields = ['id_specialities', 'name', 'description', 'registration_date', 'modification_date',
                  'registration_user', 'modification_user', 'active']


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['id_schedule', 'medical_id', 'date_of_attention', 'end_attention', 'registration_date',
                  'modification_date', 'registration_user', 'modification_user', 'active']


class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ['id_appointment', 'medical_id', 'patient_id', 'date_of_attention', 'start_of_attention',
                  'end_attention', 'state', 'observation', 'active', 'registration_date', 'registration_user',
                  'modification_date', 'modification_user']


class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = ['id_patient', 'name', 'last_name', 'dni', 'address', 'phone', 'sex', 'date_of_birth',
                  'registration_date', 'modification_date', 'registration_user', 'modification_user', 'active']


class MedicalSpecialitySerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalSpeciality
        fields = ['id_medical_speciality', 'speciality_id', 'medical_id', 'registration_date', 'modification_date',
                  'registration_user', 'modification_user', 'active']
