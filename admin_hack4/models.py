from django.db import models


class Specialty(models.Model):
    id_specialities = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=45)
    registration_date = models.DateField()
    modification_date = models.DateField()
    registration_user = models.IntegerField()
    modification_user = models.IntegerField()
    active = models.BooleanField()

    def __str__(self):
        return f'{self.name}'


class Medical(models.Model):
    id_medical = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    dni = models.CharField(max_length=45)
    address = models.CharField(max_length=45)
    email = models.CharField(max_length=45)
    phone = models.CharField(max_length=45)
    sex = models.CharField(max_length=45)
    tuition_number = models.CharField(max_length=45)
    date_of_birth = models.DateField()
    registration_date = models.DateField()
    modification_date = models.DateField()
    registration_user = models.IntegerField()
    modification_user = models.IntegerField()
    active = models.BooleanField()

    def __str__(self):
        return f'{self.name} {self.last_name}'


class MedicalSpeciality(models.Model):
    id_medical_speciality = models.IntegerField(primary_key=True)
    speciality_id = models.ForeignKey(Specialty, on_delete=models.CASCADE)
    medical_id = models.ForeignKey(Medical, on_delete=models.CASCADE)
    registration_date = models.DateField()
    modification_date = models.DateField()
    registration_user = models.IntegerField()
    modification_user = models.IntegerField()
    active = models.BooleanField()

    def __str__(self):
        return f'{self.speciality_id} {self.medical_id}'


class Schedule(models.Model):
    id_schedule = models.IntegerField(primary_key=True)
    medical_id = models.ForeignKey(Medical, on_delete=models.CASCADE)
    date_of_attention = models.DateField()
    end_attention = models.DateField()
    registration_date = models.DateField()
    modification_date = models.DateField()
    registration_user = models.IntegerField()
    modification_user = models.IntegerField()
    active = models.BooleanField()

    def __str__(self):
        return f'{self.medical_id} {self.date_of_attention}'


class Patient(models.Model):
    id_patient = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    dni = models.CharField(max_length=45)
    address = models.CharField(max_length=45)
    phone = models.CharField(max_length=45)
    sex = models.CharField(max_length=45)
    date_of_birth = models.DateField()
    registration_date = models.DateField()
    modification_date = models.DateField()
    registration_user = models.IntegerField()
    modification_user = models.IntegerField()
    active = models.BooleanField()

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Appointment(models.Model):
    id_appointment = models.IntegerField(primary_key=True)
    medical_id = models.ForeignKey(Medical, on_delete=models.CASCADE)
    patient_id = models.ForeignKey(Patient, on_delete=models.CASCADE)
    date_of_attention = models.DateField()
    start_of_attention = models.DateField()
    end_attention = models.DateField()
    state = models.CharField(max_length=45)
    observation = models.CharField(max_length=45)
    active = models.IntegerField()
    registration_date = models.DateField()
    registration_user = models.IntegerField()
    modification_date = models.DateField()
    modification_user = models.IntegerField()

    def __str__(self):
        return f'{self.medical_id} {self.patient_id}'
