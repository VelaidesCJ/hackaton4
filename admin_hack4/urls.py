from django.urls import path
from rest_framework import routers

from .views import AppointmentViewSet, MedicalViewSet, ScheduleViewSet, PatientViewSet, SpecialtyViewSet,\
    MedicalSpecialityViewSet

router = routers.DefaultRouter()
router.register(r'appointments', AppointmentViewSet)
router.register(r'medicals', MedicalViewSet)
router.register(r'schedules', ScheduleViewSet)
router.register(r'patients', PatientViewSet)
router.register(r'specialities', SpecialtyViewSet)
router.register(r'medicalSpecialities', MedicalSpecialityViewSet)

