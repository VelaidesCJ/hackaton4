import {useEffect, useState} from "react";
import Axios from "axios";
import {Button, Container, FormGroup, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";

const initialData = {
  id_patient: "",
  name: "",
  last_name: "",
  dni: "",
  address: "",
  phone: "",
  sex: "",
  date_of_birth: "",
  registration_date: "",
  modification_date: "",
  registration_user: "",
  modification_user: "",
  active: "",
};

export function Patients(){

    const [patients, setPatients] = useState([]);
    const [data, setData] = useState(initialData);
    const [modal, setModal] = useState(false);
    const [modalE, setModalE] = useState(false);
    const [amount, setAmount] = useState(0);
    const [id, setId] = useState([]);

    async function getCount(){
        let res = await Axios.get('http://localhost:8000/api/patients/');
        setAmount(res.data.count);
    }

    useEffect(()=>{
        getCount();
        Axios.get("http://localhost:8000/api/patients/").then((res)=>
        setPatients(res.data.results));
    }, []);

    const handlerData = (e) => {
        const { name, value } = e.target;
        setData({ ...data, [name]: value });
    }

    const handlerData1 = (e) => {
        setId({ ...id, [e.target.name]: e.target.value});
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        Axios
            .post("http://localhost:8000/api/patients/",
                {
                    id_patient: amount+1,
                    name: data.name,
                    last_name: data.last_name,
                    dni: data.dni,
                    address:data.address,
                    phone: data.phone,
                    sex: data.sex,
                    date_of_birth: data.date_of_birth,
                    registration_date: data.registration_date,
                    modification_date: data.modification_date,
                    registration_user: data.registration_user,
                    modification_user: data.modification_user,
                    active: data.active,
                }
                )

            .then((response) => {
                console.log(response);
                setData(initialData);
                hideModal();
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });

    };

    const handleFormSubmit1 = (e) => {
        e.preventDefault();
        Axios
            .put(`http://localhost:8000/api/patients/${id.id_patient}/`,
                {
                    id_patient: id.id_patient,
                    name: id.name,
                    last_name: id.last_name,
                    dni: id.dni,
                    address:id.address,
                    phone: id.phone,
                    sex: id.sex,
                    date_of_birth: id.date_of_birth,
                    registration_date: id.registration_date,
                    modification_date: id.modification_date,
                    registration_user: id.registration_user,
                    modification_user: id.modification_user,
                    active: id.active,
                }
                )
            .then((response) => {
                console.log(response);
                setData(initialData);
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const handleFormSubmit2 = (e) => {
        console.log(e);
        Axios
            .delete(`http://localhost:8000/api/patients/${e}/`)
            .then((response) => {
                console.log('delete successful');
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
        hideModalE();
    };

    const showModal = () => {
        setModal(true);
    }

    const hideModal = () => {
        setModal(false);
    }

    const showModalE = () => {
        setModalE(true);
    }

    const hideModalE = () => {
        setModalE(false);
    }

    function getId(id){
        setId(id);
        showModalE();
    }

    function deleteId(idid){
        handleFormSubmit2(idid);
    }

    return(
        <div className="row d-flex justify-content-center">
            <Container >
                <button color="success" onClick={showModal}> Add Patient </button>
                <Table >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Lastname</th>
                            <th>DNI</th>
                            <th>address</th>
                            <th>phone</th>
                            <th>sex</th>
                            <th>date_of_birth</th>
                            <th>registration_date</th>
                            <th>modification_date</th>
                            <th>registration_user</th>
                            <th>modification_user</th>
                            <th>active</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {patients ? patients.map((patient) => {
                            return (
                                <tr>
                                    <td>{patient.id_patient}</td>
                                    <td>{patient.name}</td>
                                    <td>{patient.last_name}</td>
                                    <td>{patient.dni}</td>
                                    <td>{patient.address}</td>
                                    <td>{patient.phone}</td>
                                    <td>{patient.sex}</td>
                                    <td>{patient.date_of_birth}</td>
                                    <td>{patient.registration_date}</td>
                                    <td>{patient.modification_date}</td>
                                    <td>{patient.registration_user}</td>
                                    <td>{patient.modification_user}</td>
                                    <td>{patient.active}</td>
                                    <td>
                                      <Button color="primary" onClick={()=> getId(patient)} >
                                        Edit
                                      </Button>{"  "}
                                      <button color="danger" onClick={()=> deleteId(patient.id_patient)}>
                                        Delete
                                      </button>
                                    </td>
                                </tr>
                            );
                        })
                            : "No data"}
                    </tbody>
                </Table>
            </Container>
            <Modal isOpen={modal}>
                <ModalHeader>
                    <div>
                        <h3> Add Patient </h3>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <label>Id:</label>
                        <input className="form-control" name="id_medical" readOnly value={patients.length+1} />
                    </FormGroup>
                    <FormGroup>
                        <label>Name:</label>
                        <input className="form-control" name="name" type="text" onChange={handlerData}/>
                    </FormGroup>
                    <FormGroup>
                        <label>Lastname:</label>
                        <input className="form-control" name="last_name" type="text" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Dni:</label>
                        <input className="form-control" name="dni" type="text" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Address:</label>
                        <input className="form-control" name="address" type="text" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Phone:</label>
                        <input className="form-control" name="phone" type="text" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Sex:</label>
                        <input className="form-control" name="sex" type="text" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Date of bird:</label>
                        <input className="form-control" name="date_of_birth" type="date" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration date:</label>
                        <input className="form-control" name="registration_date" type="date" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification date:</label>
                        <input className="form-control" name="modification_date" type="date" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration user:</label>
                        <input className="form-control" name="registration_user" type="number" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification user:</label>
                        <input className="form-control" name="modification_user" type="number" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Active:</label>
                        <input className="form-control" name="active" type="text" onChange={handlerData} />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleFormSubmit}>Add</Button>
                    <Button color="danger" onClick={hideModal}>Cancel</Button>
                </ModalFooter>
            </Modal>


            <Modal isOpen={modalE}>
                <ModalHeader>
                    <div>
                        <h3> Edit Patient </h3>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <label>Id:</label>
                        <input className="form-control" name="id_medical" readOnly onChange={handlerData} value={id.id_patient}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Name:</label>
                        <input className="form-control" name="name" type="text" onChange={handlerData1} value={id.name}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Lastname:</label>
                        <input className="form-control" name="last_name" type="text" onChange={handlerData1} value={id.last_name}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Dni:</label>
                        <input className="form-control" name="dni" type="text" onChange={handlerData1} value={id.dni}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Address:</label>
                        <input className="form-control" name="address" type="text" onChange={handlerData1} value={id.address}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Phone:</label>
                        <input className="form-control" name="phone" type="text" onChange={handlerData1} value={id.phone}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Sex:</label>
                        <input className="form-control" name="sex" type="text" onChange={handlerData1} value={id.sex}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Date of bird:</label>
                        <input className="form-control" name="date_of_birth" type="date" onChange={handlerData1} value={id.date_of_birth}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration date:</label>
                        <input className="form-control" name="registration_date" type="date" onChange={handlerData1} value={id.registration_date}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification date:</label>
                        <input className="form-control" name="modification_date" type="date" onChange={handlerData1} value={id.modification_date}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration user:</label>
                        <input className="form-control" name="registration_user" type="number" onChange={handlerData} value={id.registration_user}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification user:</label>
                        <input className="form-control" name="modification_user" type="number" onChange={handlerData1} value={id.modification_user}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Active:</label>
                        <input className="form-control" name="active" type="text" onChange={handlerData1} value={id.active}  />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleFormSubmit1}>Edit</Button>
                    <Button color="danger" onClick={hideModalE}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}