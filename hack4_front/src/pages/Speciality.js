import {useEffect, useState} from "react";
import Axios from "axios";
import {Button, Container, FormGroup, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";

const initialData = {
  id_specialities: "",
  name: "",
  description: "",
  registration_date: "",
  modification_date: "",
  registration_user: "",
  modification_user: "",
  active: "",
};

export function Speciality(){

    const [specialities, setSpecialities] = useState([]);
    const [data, setData] = useState(initialData);
    const [modal, setModal] = useState(false);
    const [modalE, setModalE] = useState(false);
    const [amount, setAmount] = useState(0);
    const [id, setId] = useState([]);

    async function getCount(){
        let res = await Axios.get('http://localhost:8000/api/specialities/');
        setAmount(res.data.count);
    }

    useEffect(()=>{
        getCount();
        Axios.get("http://localhost:8000/api/specialities/").then((res)=>
        setSpecialities(res.data.results));
    }, []);

    const handlerData = (e) => {
        const { name, value } = e.target;
        setData({ ...data, [name]: value });
    }

    const handlerData1 = (e) => {
        setId({ ...id, [e.target.name]: e.target.value});
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        Axios
            .post("http://localhost:8000/api/specialities/",
                {
                    id_specialities: amount+1,
                    name: data.name,
                    description: data.description,
                    registration_date: data.registration_date,
                    modification_date: data.modification_date,
                    registration_user: data.registration_user,
                    modification_user: data.modification_user,
                    active: data.active,
                }
                )

            .then((response) => {
                console.log(response);
                setData(initialData);
                hideModal();
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });

    };

    const handleFormSubmit1 = (e) => {
        e.preventDefault();
        Axios
            .put(`http://localhost:8000/api/specialities/${id.id_specialities}/`,
                {
                    id_specialities: id.id_specialities,
                    name: id.name,
                    description: id.description,
                    registration_date: id.registration_date,
                    modification_date: id.modification_date,
                    registration_user: id.registration_user,
                    modification_user: id.modification_user,
                    active: id.active,
                }
                )
            .then((response) => {
                console.log(response);
                setData(initialData);
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const handleFormSubmit2 = (e) => {
        console.log(e);
        Axios
            .delete(`http://localhost:8000/api/specialities/${e}/`)
            .then((response) => {
                console.log('delete successful');
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
        hideModalE();
    };

    const showModal = () => {
        setModal(true);
    }

    const hideModal = () => {
        setModal(false);
    }

    const showModalE = () => {
        setModalE(true);
    }

    const hideModalE = () => {
        setModalE(false);
    }

    function getId(id){
        setId(id);
        showModalE();
    }

    function deleteId(idid){
        handleFormSubmit2(idid);
    }

    return(
        <div className="row d-flex justify-content-center">
            <Container >
                <button color="success" onClick={showModal}> Add Speciality </button>
                <Table >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>registration_date</th>
                            <th>modification_date</th>
                            <th>registration_user</th>
                            <th>modification_user</th>
                            <th>active</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {specialities ? specialities.map((speciality) => {
                            return (
                                <tr>
                                    <td>{speciality.id_specialities}</td>
                                    <td>{speciality.name}</td>
                                    <td>{speciality.description}</td>
                                    <td>{speciality.registration_date}</td>
                                    <td>{speciality.modification_date}</td>
                                    <td>{speciality.registration_user}</td>
                                    <td>{speciality.modification_user}</td>
                                    <td>{speciality.active}</td>
                                    <td>
                                      <Button color="primary" onClick={()=> getId(speciality)} >
                                        Edit
                                      </Button>{"  "}
                                      <button color="danger" onClick={()=> deleteId(speciality.id_specialities)}>
                                        Delete
                                      </button>
                                    </td>
                                </tr>
                            );
                        })
                            : "No data"}
                    </tbody>
                </Table>
            </Container>
            <Modal isOpen={modal}>
                <ModalHeader>
                    <div>
                        <h3> Add Speciality </h3>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <label>Id:</label>
                        <input className="form-control" name="id_specialities" readOnly value={specialities.length+1} />
                    </FormGroup>
                    <FormGroup>
                        <label>Name:</label>
                        <input className="form-control" name="name" type="text" onChange={handlerData}/>
                    </FormGroup>
                    <FormGroup>
                        <label>Description:</label>
                        <input className="form-control" name="description" type="text" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration date:</label>
                        <input className="form-control" name="registration_date" type="date" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification date:</label>
                        <input className="form-control" name="modification_date" type="date" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration user:</label>
                        <input className="form-control" name="registration_user" type="number" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification user:</label>
                        <input className="form-control" name="modification_user" type="number" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Active:</label>
                        <input className="form-control" name="active" type="text" onChange={handlerData} />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleFormSubmit}>Add</Button>
                    <Button color="danger" onClick={hideModal}>Cancel</Button>
                </ModalFooter>
            </Modal>


            <Modal isOpen={modalE}>
                <ModalHeader>
                    <div>
                        <h3> Edit Speciality </h3>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <label>Id:</label>
                        <input className="form-control" name="id_specialities" readOnly onChange={handlerData} value={id.id_specialities}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Name:</label>
                        <input className="form-control" name="name" type="text" onChange={handlerData1} value={id.name}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Description:</label>
                        <input className="form-control" name="description" type="text" onChange={handlerData1} value={id.description}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration date:</label>
                        <input className="form-control" name="registration_date" type="date" onChange={handlerData1} value={id.registration_date}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification date:</label>
                        <input className="form-control" name="modification_date" type="date" onChange={handlerData1} value={id.modification_date}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration user:</label>
                        <input className="form-control" name="registration_user" type="number" onChange={handlerData} value={id.registration_user}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification user:</label>
                        <input className="form-control" name="modification_user" type="number" onChange={handlerData1} value={id.modification_user}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Active:</label>
                        <input className="form-control" name="active" type="text" onChange={handlerData1} value={id.active}  />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleFormSubmit1}>Edit</Button>
                    <Button color="danger" onClick={hideModalE}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}