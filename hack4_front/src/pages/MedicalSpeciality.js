import {useEffect, useState} from "react";
import Axios from "axios";
import {Button, Container, FormGroup, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";

const initialData = {
  id_medical_speciality: "",
  speciality_id: "",
  medical_id: "",
  registration_date: "",
  modification_date: "",
  registration_user: "",
  modification_user: "",
  active: "",
};

export function MedicalSpeciality(){

    const [medicalSpecialities, setMedicalSpecialities] = useState([]);
    const [specialities, setSpecialities] = useState([]);
    const [medicals, setMedicals] = useState([]);
    const [data, setData] = useState(initialData);
    const [modal, setModal] = useState(false);
    const [modalE, setModalE] = useState(false);
    const [amount, setAmount] = useState(0);
    const [id, setId] = useState([]);

    async function getCount(){
        let res = await Axios.get('http://localhost:8000/api/medicalSpecialities/');
        setAmount(res.data.count);
    }

    useEffect(()=>{
        getCount();
        Axios.get("http://localhost:8000/api/medicalSpecialities/").then((res)=>
        setMedicalSpecialities(res.data.results));
    }, []);

    useEffect(()=>{
        Axios.get("http://localhost:8000/api/specialities/").then((res)=>
        setSpecialities(res.data.results));
    }, []);

    useEffect(()=>{
        Axios.get("http://localhost:8000/api/medicals/").then((res)=>
        setMedicals(res.data.results));
    }, []);

    const handlerData = (e) => {
        const { name, value } = e.target;
        setData({ ...data, [name]: value });
    }

    const handlerData1 = (e) => {
        setId({ ...id, [e.target.name]: e.target.value});
    }

    const handleFormSubmit = (e) => {
        console.log('e:',e)
        e.preventDefault();
        Axios
            .post("http://localhost:8000/api/medicalSpecialities/",
                {
                    id_medical_speciality: amount+1,
                    speciality_id: data.speciality_id,
                    medical_id: data.medical_id,
                    registration_date: data.registration_date,
                    modification_date: data.modification_date,
                    registration_user: data.registration_user,
                    modification_user: data.modification_user,
                    active: data.active,
                }
                )

            .then((response) => {
                console.log(response);
                setData(initialData);
                hideModal();
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });

    };

    const handleFormSubmit1 = (e) => {
        e.preventDefault();
        Axios
            .put(`http://localhost:8000/api/medicalSpecialities/${id.id_medical_speciality}/`,
                {
                    id_medical_speciality: id.id_medical_speciality,
                    speciality_id: id.speciality_id,
                    medical_id: id.medical_id,
                    registration_date: id.registration_date,
                    modification_date: id.modification_date,
                    registration_user: id.registration_user,
                    modification_user: id.modification_user,
                    active: id.active,
                }
                )
            .then((response) => {
                console.log(response);
                setData(initialData);
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const handleFormSubmit2 = (e) => {
        console.log(e);
        Axios
            .delete(`http://localhost:8000/api/medicalSpecialities/${e}/`)
            .then((response) => {
                console.log('delete successful');
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
        hideModalE();
    };

    const showModal = () => {
        setModal(true);
    }

    const hideModal = () => {
        setModal(false);
    }

    const showModalE = () => {
        setModalE(true);
    }

    const hideModalE = () => {
        setModalE(false);
    }

    function getId(id){
        setId(id);
        showModalE();
    }

    function deleteId(idid){
        handleFormSubmit2(idid);
    }

    return(
        <div className="row d-flex justify-content-center">
            <Container >
                <button color="success" onClick={showModal}> Add Medical Speciality </button>
                <Table >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>speciality_id</th>
                            <th>medical_id</th>
                            <th>registration_date</th>
                            <th>modification_date</th>
                            <th>registration_user</th>
                            <th>modification_user</th>
                            <th>active</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {medicalSpecialities ? medicalSpecialities.map((medicalSpeciality) => {
                            return (
                                <tr>
                                    <td>{medicalSpeciality.id_medical_speciality}</td>
                                    <td>{medicalSpeciality.speciality_id}</td>
                                    <td>{medicalSpeciality.medical_id}</td>
                                    <td>{medicalSpeciality.registration_date}</td>
                                    <td>{medicalSpeciality.modification_date}</td>
                                    <td>{medicalSpeciality.registration_user}</td>
                                    <td>{medicalSpeciality.modification_user}</td>
                                    <td>{medicalSpeciality.active}</td>
                                    <td>
                                      <Button color="primary" onClick={()=> getId(medicalSpeciality)} >
                                        Edit
                                      </Button>{"  "}
                                      <button color="danger" onClick={()=> deleteId(medicalSpeciality.id_medical_speciality)}>
                                        Delete
                                      </button>
                                    </td>
                                </tr>
                            );
                        })
                            : "No data"}
                    </tbody>
                </Table>
            </Container>
            <Modal isOpen={modal}>
                <ModalHeader>
                    <div>
                        <h3> Add Medical Speciality </h3>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <label>Id:</label>
                        <input className="form-control" name="id_medical_speciality" readOnly value={medicalSpecialities.length+1} />
                    </FormGroup>
                    <FormGroup>
                        <label>Speciality:</label>
                        <select className='form-control' name='speciality_id' onChange={handlerData} >
                            {specialities
                              ? specialities.map((speciality) => {
                                  return (
                                    <option value={speciality.id_specialities} >
                                      {`${speciality.id_specialities}`}
                                    </option>
                                  );
                                })
                              : "No data"}
                        </select>
                    </FormGroup>
                    <FormGroup>
                        <label>Medical:</label>
                        <select className='form-control' name='medical_id' onChange={handlerData} >
                            {medicals
                              ? medicals.map((medical) => {
                                  return (
                                    <option value={medical.id_medical}>
                                      {`${medical.id_medical}`}
                                    </option>
                                  );
                                })
                              : "No data"}
                        </select>
                    </FormGroup>
                    <FormGroup>
                        <label>Registration date:</label>
                        <input className="form-control" name="registration_date" type="date" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification date:</label>
                        <input className="form-control" name="modification_date" type="date" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration user:</label>
                        <input className="form-control" name="registration_user" type="number" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification user:</label>
                        <input className="form-control" name="modification_user" type="number" onChange={handlerData} />
                    </FormGroup>
                    <FormGroup>
                        <label>Active:</label>
                        <input className="form-control" name="active" type="text" onChange={handlerData} />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleFormSubmit}>Add</Button>
                    <Button color="danger" onClick={hideModal}>Cancel</Button>
                </ModalFooter>
            </Modal>


            <Modal isOpen={modalE}>
                <ModalHeader>
                    <div>
                        <h3> Edit Medical Speciality </h3>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <label>Id:</label>
                        <input className="form-control" name="id_medical_speciality" readOnly onChange={handlerData} value={id.id_medical_speciality}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Speciality:</label>
                        <input className="form-control" name="speciality_id" type="text" onChange={handlerData1} value={id.speciality_id}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Medical:</label>
                        <input className="form-control" name="medical_id"  onChange={handlerData1} value={id.medical_id}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration date:</label>
                        <input className="form-control" name="registration_date" type="date" onChange={handlerData1} value={id.registration_date}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification date:</label>
                        <input className="form-control" name="modification_date" type="date" onChange={handlerData1} value={id.modification_date}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Registration user:</label>
                        <input className="form-control" name="registration_user" type="number" onChange={handlerData} value={id.registration_user}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Modification user:</label>
                        <input className="form-control" name="modification_user" type="number" onChange={handlerData1} value={id.modification_user}  />
                    </FormGroup>
                    <FormGroup>
                        <label>Active:</label>
                        <input className="form-control" name="active" type="text" onChange={handlerData1} value={id.active}  />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleFormSubmit1}>Edit</Button>
                    <Button color="danger" onClick={hideModalE}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}