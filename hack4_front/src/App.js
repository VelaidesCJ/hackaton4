import { Route, Routes } from 'react-router-dom'
import { Header } from "./components/navbar/Header";
//import {BannerMedical} from "./components/navbar/BannerMedical";
import { Medicals } from "./pages/Medicals";
import {Patients} from "./pages/Patients";
import {Speciality} from "./pages/Speciality";
import {MedicalSpeciality} from "./pages/MedicalSpeciality";


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path='/medicals' element={<Medicals />} />
          <Route path='/patients' element={<Patients />} />
          <Route path='/medicalSpeciality' element={<MedicalSpeciality />} />
          <Route path='/specialities' element={<Speciality />} />
      </Routes>
    </div>
  );
}

export default App;
