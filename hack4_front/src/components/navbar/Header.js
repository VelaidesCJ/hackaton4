import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

export function Header(){
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                {/*} <Link className="navbar-brand" to="/"> Rick and Morty </Link>*/}

                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to="/medicals">Medicals</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to="/patients">Patients</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to="/medicalSpeciality">Medical Speciality</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to="/specialities">Speciality</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    );
}