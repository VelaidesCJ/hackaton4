import bannerMedical from '../../assets/img/medical.jpg'
import bannerPatient from '../../assets/img/patient.jpg'
import bannerMedicalSpeciality from '../../assets/img/medicalSpeciality.png'
import bannerSpeciality from '../../assets/img/speciality.png'
import bannerAppointment from '../../assets/img/appoinment2.jpg'
import bannerSchedule from '../../assets/img/schedule1.jpg'
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink } from 'reactstrap';

export function BannerMedical(){
    return(
        <div>
            <NavLink href="https://www.facebook.com">
                    <img style={{ height: '300px', objectFit: 'fill' }} src={bannerMedical} className="card-img" alt="banner" />
            </NavLink>
            <NavLink href="https://www.instagram.com">
                    <img style={{ height: '300px', objectFit: 'fill' }} src={bannerPatient} className="card-img" alt="banner" />
            </NavLink>
            <NavLink href="https://www.instagram.com">
                    <img style={{ height: '300px', objectFit: 'fill' }} src={bannerMedicalSpeciality} className="card-img" alt="banner" />
            </NavLink>
            <NavLink href="https://www.instagram.com">
                    <img style={{ height: '300px', objectFit: 'fill' }} src={bannerSpeciality} className="card-img" alt="banner" />
            </NavLink>
            <NavLink href="https://www.instagram.com">
                    <img style={{ height: '300px', objectFit: 'fill' }} src={bannerAppointment} className="card-img" alt="banner" />
            </NavLink>
            <NavLink href="https://www.instagram.com">
                    <img style={{ height: '300px', objectFit: 'fill' }} src={bannerSchedule} className="card-img" alt="banner" />
            </NavLink>
        </div>

    );
}